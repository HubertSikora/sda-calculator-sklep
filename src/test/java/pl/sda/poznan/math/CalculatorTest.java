package pl.sda.poznan.math;

import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;

public class CalculatorTest {
    @Test
    public void should_return_zero_when_empty_string() {
        int sum = Calculator.sum("");
        assertEquals(0, sum);
    }

    @Test
    public void should_return_number_when_one_number_is_given() {
        int number = Calculator.sum("2");
        assertEquals(number, 2);
    }

    @Test
    public void should_sum_two_numbers() {
        int reslt = Calculator.sum("2,3");
        assertEquals(reslt, 5);
    }

    @Test
    public void should_sum_multiple_numbers() {
        int reslt = Calculator.sum("2,3,4,5");
        assertEquals(reslt, 14);
//}@Test
//public void should_sum (){
//        int reslt = Calculator.sum("")}
    }

    //zadanie 2
    @Test
    public void should_Hallo_Jan() {
        String reslt = Calculator.grate_name("jan");
        assertEquals(reslt, "Hallo Jan");
    }

    @Test
    public void ifNull() {
        String reslt = Calculator.grate_name(null);
        assertEquals(reslt, "Hello my frend");

    }

    @Test
    public void capitalCase() {
        String reslt = Calculator.grate_name("JAN");
        assertEquals(reslt, "HELLO JAN");
    }

    @Test
        public void silnia_czy_dziala() {
        {
            //AAA
            //arrange
            int n = 0;
            //act
            int result = Calculator.silnia(n);
            //assert
            assertEquals(result, 1);
        }
    }
    @Test
    public void sprawdz_pessel(){
        //AAA
        //arrange
        String pessel = "08210408725";
        //act
        boolean result = Calculator.PESEL(pessel);
        //assert
        assertEquals(result, true);
    }


}