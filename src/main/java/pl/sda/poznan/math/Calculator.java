package pl.sda.poznan.math;

public class Calculator {
    public static int sum(String str) {
        if (str.isEmpty()) {
            return 0;
        } else if (str.length() == 1) {
            return Integer.parseInt(str);

        } else {
            String[] split =  str.split(",");

            return Integer.parseInt(split[0]) + Integer.parseInt(split[1]);
        }
    }

    private static int parseInteger(String s) {
        return Integer.parseInt(s);
    }

    public static String grate_name(String s) {
        if (s.equals("Jan")) {
            return "Hallo Jan";
        } else if (s.equals("JAN")) {
            return "HELL OJAN";
        }
        return "To nie Jan. Pa!";
    }

    public static int silnia(int n) {
        int result = 1;
        if (n == 0)
            return 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    public static boolean PESEL(String pessel) {
        boolean result;
        int helper01 = 9 * convertCharToInt(pessel.charAt(0));
        int helper02 = 7 * convertCharToInt(pessel.charAt(1));
        int helper03 = 3 * convertCharToInt(pessel.charAt(2));
        int helper04 = 1 * convertCharToInt(pessel.charAt(3));
        int helper05 = 9 * convertCharToInt(pessel.charAt(4));
        int helper06 = 7 * convertCharToInt(pessel.charAt(5));
        int helper07 = 3 * convertCharToInt(pessel.charAt(6));
        int helper08 = 1 * convertCharToInt(pessel.charAt(7));
        int helper09 = 9 * convertCharToInt(pessel.charAt(8));
        int helper10 = 7 * convertCharToInt(pessel.charAt(9));
        int helper = helper01 + helper02 + helper03 + helper04 + helper05 + helper06 + helper07 + helper08 + helper09 + helper10;
        helper = helper % 10;
        if (helper == convertCharToInt(pessel.charAt(10))) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    private static int convertCharToInt(char c) {
        return Integer.parseInt(String.valueOf(c));
    }
}